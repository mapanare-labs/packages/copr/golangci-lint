%global debug_package %{nil}

Name:    golangci-lint
Version:        1.62.2
Release: 1%{?dist}
Summary: Linters Runner for Go
Group:   Applications/System
License: GPLv3+
Url:     https://github.com/golangci/%{name}
Source0: %{url}/releases/download/v%{version}/%{name}-%{version}-linux-amd64.tar.gz

%description
GolangCI-Lint is a linters aggregator. It's fast: on average 5 times faster than
gometalinter. It's easy to integrate and use, has nice output and has a minimum
number of false positives. It supports go modules.

GolangCI-Lint has integrations with VS Code, GNU Emacs, Sublime Text.

%prep
%setup -qn %{name}-%{version}-linux-amd64

%build
# Building completions
./%{name} completion bash > completion.bash
./%{name} completion zsh > completion.zsh
./%{name} completion fish > completion.fish

%install
install -d -m 755 %{buildroot}%{_bindir}
install -m 755 %{name} %{buildroot}%{_bindir}

# Completions
install -Dpm 644 completion.bash %{buildroot}%{_datadir}/bash-completion/completions/%{name}
install -Dpm 644 completion.zsh %{buildroot}%{_datadir}/zsh/site-functions/_%{name}
install -Dpm 644 completion.fish %{buildroot}%{_datadir}/fish/vendor_completions.d/%{name}.fish

%files
%license LICENSE
%doc *.md
%{_bindir}/%{name}
%{_datadir}/bash-completion/completions/%{name}
%{_datadir}/zsh/site-functions/_%{name}
%{_datadir}/fish/vendor_completions.d/%{name}.fish

%changelog
* Mon Dec 09 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Mon Jun 10 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Tue May 28 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Wed Apr 10 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Sun Mar 03 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 1.56.2

* Wed Nov 15 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 1.55.2

* Thu Aug 31 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 1.54.2

* Mon Jun 19 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 1.53.3

* Tue Apr 25 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 1.52.2

* Sat Feb 25 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 1.51.2

* Fri Feb 10 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 1.51.1

* Tue Nov 01 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 1.50.1

* Fri Sep 16 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Adding bash, zsh and fish completions

* Mon Sep 12 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Initial RPM
